var cors = require('cors');
var currentUser = require('../data/userLogged.js').currentUser;
var results = require('../data/searchResult.js').results;

module.exports = function(app) {
    
    var routes = {
        configRoutes: function () {
            app.post('/', cors(), function(req, res, next){
                res.json({
                    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJzdWIiOiI1NGE4Y2U2MThlOTFiMGIxMzY2NWUyZjkiLCJpYXQiOiIxNDI0MTgwNDg0IiwiZXhwIjoiMTQyNTM5MDE0MiJ9yk4nouUteW54F1HbWtgg1wJxeDjqDA_8AhUPyjE5K0U"
                });
            });

            app.get('/user/:id/information', function(req, res, next){
                res.json(currentUser);
            });

            app.put('/user/information', cors(), function(req, res, next){
                currentUser = req.body;
                res.json({
                    message: "success"
                });
            });

            app.get('/search/:query/result', function(req, res, next){
                res.json(results);
            });
            
        }
    }
    
    return routes;
}