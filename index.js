var express = require('express')
	, app = express();

var routes = require('./routes/routes.js');
var config = require('./config/config.js');

var configApp = config(app);
configApp.configApp();

var routesApp = routes(app);
routesApp.configRoutes();


app.listen(3000, function(){
	console.log('Web server listening on port 3000');
});
