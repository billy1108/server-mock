var bodyParser = require("body-parser")

module.exports = (app) => {
    return {
      configApp: () => {

		app.use(function(req, res, next) {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Methods",  "POST, GET, PUT");
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
			next();
		});

		app.use(bodyParser.json());

      }  
    };
}